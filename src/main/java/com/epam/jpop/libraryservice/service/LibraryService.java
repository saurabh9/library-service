package com.epam.jpop.libraryservice.service;

import com.epam.jpop.libraryservice.client.BookServiceClient;
import com.epam.jpop.libraryservice.client.UserServiceClient;
import com.epam.jpop.libraryservice.model.Book;
import com.epam.jpop.libraryservice.model.Library;
import com.epam.jpop.libraryservice.model.User;
import com.epam.jpop.libraryservice.repository.LibraryRepository;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service
public class LibraryService {

    @Inject
    LibraryRepository libraryRepository;

    @Inject
    private BookServiceClient bookServiceClient;

    @Inject
    private UserServiceClient userServiceClient;

    public ResponseEntity<List<Book>> getBooks() {

        return bookServiceClient.getBooks();
    }

    public ResponseEntity<Book> getBook(Long id) {

        return bookServiceClient.getBook(id);
    }

    public void updateUser(Long id, User user) {

        userServiceClient.updateUser(user, id);
    }

    public ResponseEntity<User> addUser(User user) {

        return userServiceClient.saveUser(user);
    }

    public void createEntry(Long userId, Long bookId) {

        userServiceClient.getUser(userId);
        bookServiceClient.getBook(bookId);

        Library library = new Library();
        library.setBookId(bookId);
        library.setUserId(userId);

        libraryRepository.save(library);

    }

    public ResponseEntity<Book> addBook(Book book) {

        return bookServiceClient.saveBook(book);
    }

    public void deleteBook(Long id) {
        bookServiceClient.deleteBook(id);
    }

    public void deleteUser(Long id) {
        userServiceClient.deleteUser(id);
    }

    public ResponseEntity<User> getUser(Long id) {
        return userServiceClient.getUser(id);
    }

}
