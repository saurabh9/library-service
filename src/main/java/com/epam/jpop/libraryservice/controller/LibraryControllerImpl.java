package com.epam.jpop.libraryservice.controller;

import com.epam.jpop.libraryservice.model.Book;
import com.epam.jpop.libraryservice.model.User;
import com.epam.jpop.libraryservice.service.LibraryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

@RestController
public class LibraryControllerImpl implements LibraryResource {

    @Inject
    LibraryService libraryService;

    @Override
    public ResponseEntity<List<Book>> getBooks() {
        return libraryService.getBooks();
    }

    @Override
    public ResponseEntity<Book> getBook(Long id) {
        return libraryService.getBook(id);
    }

    @Override
    public ResponseEntity<Object> updateUser(Long id, User user) {
        libraryService.updateUser(id, user);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<User> addUser(User user) {
        return libraryService.addUser(user);
    }

    @Override
    public ResponseEntity<Object> createEntry(Long userId, Long bookId) {
        libraryService.createEntry(userId, bookId);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Book> addBook(Book book) {
        return libraryService.addBook(book);
    }

    @Override
    public ResponseEntity<Object> deleteBook(Long id) {
        libraryService.deleteBook(id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Object> deleteUser(Long id) {
        libraryService.deleteUser(id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<User> getUser(Long id) {
        return libraryService.getUser(id);
    }
}
