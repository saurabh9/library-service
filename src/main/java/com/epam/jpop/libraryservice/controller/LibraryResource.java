package com.epam.jpop.libraryservice.controller;

import com.epam.jpop.libraryservice.model.Book;
import com.epam.jpop.libraryservice.model.User;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(path = "/library")
@Api(value = "Book Resource", description = "Contains all operations concerning library")
public interface LibraryResource {

    @ApiOperation(value = "View a list of available books", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list")
    })
    @GetMapping(path = "/books")
    ResponseEntity<List<Book>> getBooks();

    @ApiOperation(value = "View a particular book", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved book"),
            @ApiResponse(code = 402, message = "Library error")
    })
    @GetMapping(path = "/book/{id}")
    ResponseEntity<Book> getBook(
            @ApiParam(value = "Book ID from which the book will be retrieved", required = true) @PathVariable("id") Long id);

    @ApiOperation(value = "Update user", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated user"),
            @ApiResponse(code = 402, message = "Library error")
    })
    @PutMapping(path = "/user/{id}")
    ResponseEntity<Object> updateUser(
            @ApiParam(value = "User id according to which user will be updated", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "user details", required = true) @RequestBody User user);

    @ApiOperation(value = "Add new user", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully added user")
    })
    @PostMapping(path = "/user")
    ResponseEntity<User> addUser(
            @ApiParam(value = "user details", required = true) @RequestBody User user);

    @ApiOperation(value = "Issue a book to particular user", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully issued"),
            @ApiResponse(code = 404, message = "Not Found")
    })
    @PutMapping(path = "/user/{userId}/book/{bookId}")
    ResponseEntity<Object> createEntry(
            @ApiParam(value = "user id", required = true) @PathVariable("userId") Long userId,
            @ApiParam(value = "book id", required = true) @PathVariable("bookId") Long bookId);

    @ApiOperation(value = "Add new book", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully added book")
    })

    @PostMapping(path = "/book")
    ResponseEntity<Book> addBook(
            @ApiParam(value = "book details", required = true) @RequestBody Book book);


    @ApiOperation(value = "Delete book", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted book")
    })

    @DeleteMapping(path = "/book/{id}")
    ResponseEntity<Object> deleteBook(
            @ApiParam(value = "book id", required = true) @PathVariable("id") Long id);

    @ApiOperation(value = "Delete user", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted book")
    })

    @DeleteMapping(path = "/user/{id}")
    ResponseEntity<Object> deleteUser(
            @ApiParam(value = "user id", required = true) @PathVariable("id") Long id);

    @ApiOperation(value = "View a particular user", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved user"),
            @ApiResponse(code = 404, message = "Not Found")
    })
    @GetMapping(path = "/user/{id}")
    ResponseEntity<User> getUser(
            @ApiParam(value = "User ID from which the user will be retrieved", required = true) @PathVariable("id") Long id);

}
