package com.epam.jpop.libraryservice.client;

import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class BookServiceClientFactory implements FallbackFactory<BookServiceClient> {

    @Override
    public BookServiceClient create(Throwable throwable) {
        return new BookServiceClientFallback(throwable);
    }
}
