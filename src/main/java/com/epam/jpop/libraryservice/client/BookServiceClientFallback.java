package com.epam.jpop.libraryservice.client;

import com.epam.jpop.libraryservice.exception.LibraryException;
import com.epam.jpop.libraryservice.model.Book;
import feign.FeignException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class BookServiceClientFallback implements BookServiceClient {

    private static final Logger LOG = LoggerFactory.getLogger(BookServiceClientFallback.class);
    private final Throwable cause;

    public BookServiceClientFallback(Throwable cause) {
        this.cause = cause;
    }

    @Override
    public ResponseEntity<List<Book>> getBooks() {
        LOG.warn("Unable to get book");
        return null;
    }

    @Override
    public ResponseEntity<Book> getBook(Long id) {
        if (checkNotFound()) {
            throw new LibraryException("404 error took place when getBook was called with bookId: "
                    + id + ". Error message: "
                    + cause.getLocalizedMessage());
        } else {
            LOG.warn("Unable to get book");
        }
        return null;
    }

    private boolean checkNotFound() {
        return cause instanceof FeignException && ((FeignException) cause).status() == 404;
    }

    @Override
    public ResponseEntity<Book> saveBook(Book book) {
        LOG.warn("Unable to save book");
        return null;
    }

    @Override
    public void deleteBook(Long id) {
        if (checkNotFound()) {
            throw new LibraryException("404 error took place when deleteBook was called with bookId: "
                    + id + ". Error message: "
                    + cause.getLocalizedMessage());
        } else {
            LOG.warn("Unable to delete book");
        }
    }

    @Override
    public void updateBook(Book book, Long id) {
        if (checkNotFound()) {
            throw new LibraryException("404 error took place when updateBook was called with bookId: "
                    + id + ". Error message: "
                    + cause.getLocalizedMessage());
        } else {
            LOG.warn("Unable to update book");
        }
    }
}
