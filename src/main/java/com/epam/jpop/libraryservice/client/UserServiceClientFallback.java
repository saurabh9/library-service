package com.epam.jpop.libraryservice.client;

import com.epam.jpop.libraryservice.exception.LibraryException;
import com.epam.jpop.libraryservice.model.User;
import feign.FeignException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import javax.inject.Named;

public class UserServiceClientFallback implements UserServiceClient {

    private static final Logger LOG = LoggerFactory.getLogger(UserServiceClientFallback.class);
    private final Throwable cause;

    public UserServiceClientFallback(Throwable cause) {
        this.cause = cause;
    }

    @Override
    public ResponseEntity<User> getUser(Long id) {
        if (checkNotFound()) {
            throw new LibraryException("404 error took place when getUser was called with userId: "
                    + id + ". Error message: "
                    + cause.getLocalizedMessage());
        } else {
            LOG.warn("Error in getting User");
        }
        return null;
    }

    @Override
    public ResponseEntity<User> saveUser(User user) {
        LOG.warn("Error in saving User");
        return null;
    }

    @Override
    public void deleteUser(Long id) {
        if (checkNotFound()) {
            throw new LibraryException("404 error took place when deleteUser was called with userId: "
                    + id + ". Error message: "
                    + cause.getLocalizedMessage());
        } else {
            LOG.warn("Error in deleting User");
        }
    }

    @Override
    public void updateUser(User user, Long id) {
        if (checkNotFound()) {
            throw new LibraryException("404 error took place when updateUser was called with userId: "
                    + id + ". Error message: "
                    + cause.getLocalizedMessage());
        } else {
            LOG.warn("Error in updating User");
        }
    }

    private boolean checkNotFound() {
        return cause instanceof FeignException && ((FeignException) cause).status() == 404;
    }
}
