package com.epam.jpop.libraryservice.client;

import com.epam.jpop.libraryservice.model.Book;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.inject.Named;
import java.util.List;

@FeignClient(name = "book-service", fallbackFactory = BookServiceClientFactory.class)
@Component
public interface BookServiceClient {

    @GetMapping("/book")
    ResponseEntity<List<Book>> getBooks();

    @GetMapping("/book/{id}")
    ResponseEntity<Book> getBook(@PathVariable("id") Long id);

    @PostMapping("/book")
    ResponseEntity<Book> saveBook(@RequestBody Book book);

    @DeleteMapping("/book/{id}")
    void deleteBook(@PathVariable("id") Long id);

    @PutMapping("/book")
    void updateBook(@RequestBody Book book, @PathVariable("id") Long id);
}
