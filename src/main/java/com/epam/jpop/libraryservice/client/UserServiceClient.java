package com.epam.jpop.libraryservice.client;

import com.epam.jpop.libraryservice.model.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "user-service", fallbackFactory = UserServiceClientFactory.class)
@Component
public interface UserServiceClient {

    @GetMapping("/user/{id}")
    ResponseEntity<User> getUser(@PathVariable("id") Long id);

    @PostMapping("/user")
    ResponseEntity<User> saveUser(@RequestBody User user);

    @DeleteMapping("/user/{id}")
    void deleteUser(@PathVariable("id") Long id);

    @PutMapping("/user")
    void updateUser(@RequestBody User user, @PathVariable("id") Long id);
}
