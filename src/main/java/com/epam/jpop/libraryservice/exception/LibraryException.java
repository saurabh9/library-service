package com.epam.jpop.libraryservice.exception;

public class LibraryException extends RuntimeException {

    public LibraryException(String message) {
        super(message);
    }
}
